\section{Why Germany gets it wrong about antisemitism and Palestine}
\sectionmeta{Inna Michaeli}{Inna Michaeli, \textit{Open Democracy}, 2021}

\noindent
Palestine solidarity demonstrations and actions in Germany have been
accused of antisemitism, yet when we ask what was actually antisemitic
about them, it turns out not to be antisemitism at all. Let me explain
why. What I am offering here is a public service, a Jewish queer woman's
perspective for the German media, politicians and public.

In Germany, defending the right of the Palestinian people to exist, to
live in safety and dignity in their homeland, is regularly met with
accusations of antisemitism. But these accusations have little to do
with Jews, and everything to do with a German-centred view of the world,
and racism against Palestinians, Muslims and migrants in Germany and
across Europe.

German politicians speak day and night of Germany's commitment to
eradicate antisemitism and preserve Jewish life. One form this
commitment takes is unconditional diplomatic, military and financial
support to Israel, even as it commits war crimes in Gaza and maintains a
regime recently recognised by
\href{https://www.hrw.org/report/2021/04/27/threshold-crossed/israeli-authorities-and-crimes-apartheid-and-persecution}{Human
Rights Watch} as apartheid. Another way to demonstrate this commitment
is to defame individuals and organizations who support human rights for
Palestinians and resist the Israeli apartheid regime as antisemitic,
even if these individuals or the members of these organisations are
themselves Jewish. Yet another is to blame migrants and refugees for
``imported antisemitism'', positioning Germany as the protector of the
Jews from antisemitism, the source of which by implication lie outside
of Germany.

Take for example Berlin's interior minister, Andreas Geisel. In a 2019
\href{https://www.zeit.de/politik/deutschland/2019-09/andreas-geisel-innensenator-berlin-bds-bewegung-verfassungsschutz}{interview}
with Die Zeit, the SPD politician stated: ``When I hear that BDS {[}the
boycott, divestment and sanctions campaign against Israel{]} supposedly
has a commitment to oppose antisemitism, I can only smile wearily. Such
organizations like to claim that they are anti-Zionist, but not
antisemitic. In practice, BDS is hostile to Israel. There, the
transitions to antisemitism are fluid.'' Contrary to Geisel's conflation
of antisemitism with anti-Zionism, there is nothing fluid here, nor a
conspiracy to mean something other than what they say.

While for some white supremacists Israel represents the centre of the
global Jewish conspiracy and a force of evil (the way they understand
it, of course), others are quite fond of it. Far right movements and
politicians can often profess support for Israel, while at the same time
promoting or tolerating antisemitic views. Think of the far-right
\href{https://www.haaretz.com/world-news/europe/.premium-how-germany-s-far-right-party-is-luring-jewish-voters-in-this-year-s-election-1.9478145}{AfD
party} in Germany, the Hungarian prime minister
\href{https://www.politico.eu/article/viktor-orban-anti-semitism-problem-hungary-jews/}{Viktor
Orbán} in Hungary, or Donald Trump. On the other hand, movements like
Palästina Spricht (``Palestine speaks'') collaborate with many Jews,
\href{https://www.facebook.com/PalastinaSpricht/posts/1368407210209694}{speaking
out against antisemitism} with an integrity that state institutions can
only dream of. They make clear beyond doubt that antisemites aren't
welcome, and that Zionism doesn't equate with Judaism; that Jews as a
whole are in no way responsible for the crimes of the Israeli state.

Palestinians and those who support them are not the ones unable to
distinguish between racism against Jews and resistance to Israel. It's
politicians like Geisel and Michael Müller, Berlin's mayor, who
conveniently compared the non-violent and anti-racist BDS movement
\href{https://www.jpost.com/diaspora/under-fire-berlin-mayor-equates-bds-with-nazis-rejects-israel-boycott-504425}{to
the Nazis}.

It is widely assumed that due to its history, Germany is particularly
sensitive to antisemitism. However, although German public figures and
state institutions invoke antisemitism as an argument against
Palestinian rights, they are rarely sensitive to the Jewish experience,
or to racism at all. On the contrary, they seem to be exclusively
attuned to white German history, experience, and cultural and emotional
associations --- in other words, sensitive to themselves.

The popular narrative speaks of German guilt, but it is not the kind of
guilt that comes with decentring oneself and recognising the other --- it
is a self-centered, narcissistic position. The result is a form of
politics that effectively leads to death and destruction in Palestine,
and to attempts to criminalise Palestine solidarity and act violently
towards migrants in Germany itself. For a state so preoccupied with
``integration'', Germany works hard to alienate us.

It is easy to see how this worldview results in the police brutality we
then experience at demonstrations. On top of the classic police
violence, this particular brutality is also politically motivated,
serving the agenda to crush the Palestine solidarity movement. Yet it's
a growing, popular movement which is stronger than ever before in Berlin
and the rest of Germany.

\subsection{Kindermörder Israel}

Slogans like \emph{Kindermörder Israel} (``child-killer Israel'') are a
description of horrendous reality --- one of three Palestinians that
Israel kills in Gaza are children. As inconvenient and triggering as it
is for many Germans (and quite a few Jews), what should people chant
when Israel is killing children? How can the victims express their rage
and sorrow, how can they mourn their children who are killed again and
again by Israel?

It appears that this particular slogan upsets some people for two
reasons. One is the supposed equation between Jews and Israel. So when
you say ``Israel'', the listener hears ``Jews'', regardless of the
context of what is said and by whom. But a listener who thinks of Jews
as synonymous with Israel is the one with the antisemitism problem, not
the slogan.

One way to avoid this problem, suggested a commenter on a recent German
Facebook thread, would be to say ``Netanyahu'' instead of Israel.
Netanyahu certainly has blood on his hands, but not only Netanyahu.
Israel killed children in Gaza before Netanyahu and will likely kill
after him too --- with the blessing of Germany and the European Union.

It's not a single person, it's an entire machinery. It is the Israeli
education system that educates children from kindergarten onwards to
become soldiers, it is the mandatory military service that creates a
militarised society, it is the media that aligns with the army and
always provides the ideological justification in advance for any war
crime. It is a culture that perpetuates a permanent sense of victimhood,
that denies the Nakba and the occupation, that dehumanises Palestinians,
that sends its youth to occupy, shoot, and kill. Israel is doing the
killing, state and society.

The second reason that such a slogan upsets people is the centuries-old
antisemitic blood libel, the accusation that Jews kill Christian
children to use their blood, found in the history of different regions
but dominant primarily in Europe. It is indeed a terrible antisemitic
trope. Yet Christian European history is not a universal reference
point. It is a reference point of a particular ethnic and religious
group in Germany, namely white Germans with Christian heritage. It is
also, naturally, a reference point for Jews in Germany.

In Germany, different ethnic and religious communities have different
historical trajectories and cultural associations, and the expectation
for everyone to share what is primarily Christian European sensibilities
and associations is problematic. For Palestinian refugees who came to
Germany well after the Holocaust, for reasons not unrelated to it,
``childkiller Israel'' invokes associations to the Palestinian children
killed by the Israeli military and policies, rather than the blood
libel. Nor is this blood libel any central reference point for Israeli
Jews, who grew up and were socialized in Israel. Yes, this is the
primary association for those who grew up in or next to Christian
European tradition --- but the story here is not about them. Decentering
oneself and one's particular cultural associations and emotional
landscape as the universal reference point is the task at hand facing
the German society. It is, in other worlds, learning to say: it's not
about me.

\subsection{Israeli flag burning}

On 15 May, the day that the Nakba --- the 1948 expulsion and displacement
of Palestinians from the newly-declared state of Israel --- is
commemorated, Germany saw
\href{https://www.palaestinaspricht.de/news/nakba2021}{possibly the
largest-ever demonstrations of solidarity} with the Palestinian people.
It was impossible to ignore how intersectional those demonstrations
were, from the Latin American bloc to intersectional feminists.

Yet the Guardian's
\href{https://www.theguardian.com/world/2021/may/17/anti-israel-protests-in-germany-prompt-calls-for-antisemitism-crackdown}{report
on the protests} chose instead to highlight German politicians'
condemnation of alleged antisemitism, ignoring speeches by Jewish
activists and groups like \href{https://fb.watch/5zhA3CFhLo/}{Jewish
Voice} or the \href{https://fb.watch/5zhIyyD1u0/}{Jewish Bund}, and
focusing instead on such horrors as the burning of an Israeli flag. Much
of the mainstream media coverage of Nakba Day demonstrations did not
even mention nor explain to the readers what the Nakba is, and its
continuation in the form of ethnic cleansing and denial of Palestinians'
right to return. Berlin, with the largest Palestinian population in
Europe, is home to people whose family members have been murdered by
Israel in the recent days. These protests are often framed as ``anti''
Israel, but the fact that they are primarily ``for'' Palestinian life is
omitted.

This is exemplary of public discourse on the issue in Germany, the
United Kingdom and elsewhere: Israeli flags matter, Palestinian lives do
not. When people, politicians and the media, care more about the burning
of national flags than the burning of homes and neighbourhoods and the
killing of entire families, they should really have a hard look at
themselves.

Here too, in the eyes of the beholder, Israeli flag stands for Jews (and
the beholder assumes everyone shares their associations). You can be
sentimental about the Star of David as much as you want, but painted on
a house in Sheikh Jarrah, it is not more than a symbol of violence and
ethnic cleansing. Painted on the Israeli flag, it is a symbol of
colonization, occupation and an apartheid regime.

During Channukah 2017, the Jewish Antifa Berlin group staged a
\href{https://www.facebook.com/JewishAntiFaBerlin/photos/1795622000447995}{Chanukkia}
with the words: ``on our Chanukia, instead of candles, there are now the
symbols of human bondage --- the national flags of repressive regimes
from all over the world, which, in their own unique ways, are
responsible for global misery. Their sacralisation is the modern form of
idolatry.'' Contrary to what some German politicians think, not all Jews
are the same.

\subsection{From the River to the Sea, Palestine will be Free}

Another slogan that's stressful to many German ears --- and many Jewish
ones --- is ``from the River to the Sea, Palestine will be Free''. Like
the hollow expression
\href{https://jacobinmag.com/2021/05/israel-palestine-right-to-exist-gaza}{``Israel's
right to exist''}, it evokes the fear that Jews will be annihilated, if
they cannot maintain a state where Jews control the territory between
the river Jordan and the Mediterranean. For many people, no Israel means
no Jews. Feeding this fear is to perpetuate the logic that Jewish life
depends on racial and ethno-national demographic and political
domination of one group over another, rather than more egalitarian and
democratic frameworks of civil rights, in all their imperfection.

Yet open antisemites who conflate Israel and Jews are not the only ones
to do so. The Israeli state does its best efforts to position itself as
the voice of the world's Jewish communities. The German media often
unthinkingly describes Israel as ``the Jewish state'', even though
democratic principles would suggest that a state should not be
ethnically or religiously exclusive.

Tragically, mainstream Jewish institutions in many countries also align
themselves with Israeli politics no matter what, and wave Israeli flags
at every opportunity, including at the very moments that bombs fall and
kill entire families in Gaza. This makes the task of distinguishing
between Israel and Jews all the more difficult --- yet an anti-racist
position demands that we do.

There is also a deeper political and philosophical question here. What
does it even mean, for Israel to exist? I was a child when my family
immigrated to Israel, I have Israeli citizenship, and I grew up in the
Israeli education system. I didn't know where Palestine was and what
exactly it was until I was an adult.

I grew up in Haifa not knowing it was and is a Palestinian city. Right
or no right, Israel does exist for me --- as a nation-state, as a system,
as a society that made much of who I am. It exists as a colonial project
and a machinery of oppression. Yet, this very land on which Israel
exists and which it constantly tries to grab, occupy and take control
of, is Palestine.

My understanding of this land is neither religious nor essentialist, but
political. I understand the land between the river and the sea to be
Palestine, colonised. This understanding comes out of respect for what
the land means to people who were colonised and displaced. Refusing to
recognise this geopolitical space as Palestine would contribute to its
colonisation and complete its erasure. This we must refuse. So for me,
Haifa is Palestine, even if it is simultaneously the Israel I grew up
in.

Palestine exists as a land, a country, and also as an idea of freedom,
homecoming and decolonisation. It also exists as a Palestinian society,
in diaspora and in Palestine. It has different degrees of overlap with
the Jewish-Israeli one, and exists in different constellations of
colonial control, from the besieged Gaza to the occupied West Bank to
the \href{https://www.jadaliyya.com/Details/40310}{Israeli citizenship
of Palestinians}.

\subsection{Jewish life doesn't require Palestinian death}

Ultimately, this Israeli apartheid system draws its legitimacy, among
others, from telling Jews in Palestine and around the world, that if it
collapses, so will they. Germany also gets this message loud and clear.
South African Apartheid relied on a similar myth, convincing its allies
in Britain, the US, Germany and elsewhere, that its fall would trigger
an annihilation of whites by Blacks.

Yet what if it isn't true? What if one day this system collapses --- the
same way the Soviet Union collapsed, or the German Democratic Republic,
or Czechoslovakia --- and we are left very much alive and breathing?

The rockets from Hamas are invoked --- not just by Israel but also by
Germany, the EU and the US --- to justify the death and destruction in
Gaza. As if Gaza has to burn for Jews to live.

What if Jewish life doesn't require Palestinian death? (Though even if
it did, my life certainly isn't worth more than the life of Rajaa Abu
Al-Ouf, a dedicated social worker and psychologist who worked to provide
children with psychological support, murdered last week in Gaza, with
her children.)

What if Jewish existence doesn't mean that as Jews we necessarily have
to become occupiers, colonisers, \emph{Kindermörder}? What if the
expectation placed on us to become these things is itself the worst
antisemitic blood libel of all?

If you choose death in Gaza, you don't choose life for anyone. You
choose death.

Consider the Israelis on the Gaza border who suffer from the rockets.
The moment a resident of the south or elsewhere refuses to serve as
justification for the massacre in Gaza and starts to talk of a peaceful
solution, they are often branded as
\href{https://www.theguardian.com/world/2019/mar/14/the-fall-of-the-israeli-peace-movement-and-why-leftists-continue-to-fight}{traitors}
(You note quite rightly that I am not talking about Hamas here, because
when it wasn't Hamas or Islamic Jihad, it was someone else. You cannot
hold millions of people in the world's largest prison, and expect
they'll throw flowers at you.)

Israel and the international community had the chance, when Hamas was
democratically elected, to enable them transition from a militarised
group to political actors. Plenty of governments started as `terror'
groups. Nelson Mandela was
\href{https://time.com/5338569/nelson-mandela-terror-list/}{listed} on
the terror watchlist in the US until 2008. In Germany, he was considered
a
`\href{https://www.dw.com/en/germany-looks-back-at-its-mandela-skeptic-past/a-17277862}{state
terrorist}' for a long time by successive German governments. Clearly
the interest here was not to allow any legitimate Palestinian
sovereignty.

This is also my appeal to fellow Jews in Germany and beyond. I know our
intergenerational traumas. Many of us know perfectly well from our
grandparents' generation what it means to have your entire family
murdered. How loss and trauma and yes, fear, continue to live in the
next generations.

Fear is a powerful instrument that is used to control people --- so let
us not be controlled. Let's be afraid of what we really need to be
afraid of: white supremacy and colonialism, fascism and nationalism,
murderous regimes and apartheid. Even if these things come under the
name of Israel.

As a Jewish queer woman, I know that I am genuinely safer with my
Palestinian friends and comrades, than with the German establishment. I
am convinced we must work against antisemitism and all forms of racism,
with those affected by it --- not by finding fake comfort in white
saviours. As long as Jews turn to the white German establishment for
safety, this will never stop the paralysing fear and anxiety, because we
have excellent reasons not to trust this establishment with our
lives.The establishment that lets thousands of refugees drown in the
Mediterranean and shakes hands with Israeli war criminals before it is
willing to speak with critical Jews.

So let's take a deep breath and together: from the river to the sea,
Palestine will be free.
